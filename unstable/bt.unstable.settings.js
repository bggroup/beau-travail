floodEnds={
	'default':[
	function(scale,options){
		if(scale.floods[options.id])return false
			return true
	},
	function(scale,options){
		if(!options.lifeTime)return false
			return true
	},
	function(scale,options){
		for(var s in options.scope)if(scale.datas[options.scope[s]])return false
			return true
	},
	function(scale,options){
		if(options.bool)floodMobiles(options,scale.mobiles,options.path);
		return true
	}],
	'log':[
	function(scale,options){
		if(scale.floods[options.id]){
			if(!options.bool){
				scale.floods[options.id]=false;
			}else{
				return false
			}
		}
		return true
	},
	function(scale,options){
		if(!options.lifeTime)return false
			return true
	},
	function(scale,options){
		for(var s in options.scope)if(scale.datas[options.scope[s]])return false
			return true
	}],
	'drop':[
	function(scale,options){
		if(scale.floods[options.id])return false
			return true
	},
	function(scale,options){
		if(!options.lifeTime)return false
			return true
	},
	function(scale,options){
		for(var s in options.scope)if(scale.datas[options.scope[s]])return false
			return true
	},
	function(scale,options){
		if(options.owner.inventory.length>0 && !scale.datas[options.owner.inventory[0]]){
			options.data=options.owner.inventory[0];
			options.owner.inventory.splice(0,1);
			return true
		}else{
			options.lifeTime++;
			return false
		}
	}],
	'vacuum':[
	function(scale,options){
		if(scale.floods[options.id])return false
			return true
	},
	function(scale,options){
		if(!options.lifeTime)return false
			return true
	},
	function(scale,options){
		for(var s in options.scope)if(scale.datas[options.scope[s]])return false
			return true
	},
	function(scale,options){
		if(options.owner.inventory.length>options.owner.capacity){
			return false
		}
		if(scale.datas[options.data]){
			floodMobiles({data:options.data},[options.owner]);
		}
		return true
	}],
	'light':[
	function(scale,options){
		if(scale.floods[options.id])return false
			return true
	},
	function(scale,options){
		if(!options.lifeTime)return false
			return true
	},
	function(scale,options){
		for(var s in options.scope)if(scale.datas[options.scope[s]]){
			scale.fill({data:options.data,bool:options.bool});
			return false
		}
		return true
	}],
	'default.rdm':[
	function(scale,options){
		if(scale.floods[options.id])return false
			return true
	},
	function(scale,options){
		if(!options.lifeTime)return false
			return true
	},
	function(scale,options){
		for(var s in options.scope)if(scale.datas[options.scope[s]])return false
			return true
	},
	function(scale,options){
		var rdm=Math.random();
		if(rdm>options.rdm)return false
			return true
	}],
}

floodFills={
	'default':{
		positions:[],
		lifeTime:Infinity,
		dir:[[-1,0,0],[1,0,0],[0,-1,0],[0,1,0],/*[0,0,1],[0,0,-1]*/],
		floodEnd:floodEnds['default'],
		speed:50,
		scope:[],
		bool:true,
		parsed:true,
	}
}

function animation(options){
	if(!options.delay)options.delay=100;
	if(!options.position)options.position = options.mobile.position;
	options.position.floodFill(options);
	if(options.data=='vacuum'){console.log(options.lifeTime)}
		setTimeout(function(){
			options.bool=false;
			options.position.floodFill(options);
		},options.delay);
}

function event(options){
	if(!options.delay)options.delay=100;
	if(options.animation)animations[options.animation](options.mobile);
	var position = options.mobile.position;
	options.owner = options.mobile;
	options.mobile.position.floodFill(options);
	setTimeout(function(){
		options.bool=false;
		position.floodFill(options);
	},options.delay);
}

animations={
	'jump':function(mobile,dir){
		animation({mobile:mobile,dir:dir,data:'jump',speed:17,lifeTime:mobile.jump});
	},
	'hit':function(mobile){
		animation({mobile:mobile,data:'hit',speed:1,lifeTime:5});
	},
	'danger':function(mobile){
		animation({mobile:mobile,data:'danger',speed:1,lifeTime:1});
	},
	'positif':function(mobile){
		animation({mobile:mobile,data:'positif',speed:1,lifeTime:1});
	},
	'negatif':function(mobile){
		animation({mobile:mobile,data:'negatif',speed:1,lifeTime:1});
	},
	'fire':function(mobile){
		animation({mobile:mobile,data:'hit',speed:1,lifeTime:1});
	},
	'drop':function(mobile){
		animation({mobile:mobile,data:'drop',speed:17,lifeTime:mobile.floodFill.lifeTime,dir:mobile.floodFill.dir});
	}
	,
	'vacuum':function(mobile){
		animation({mobile:mobile,data:'vacuum',speed:1,lifeTime:mobile.floodFill.lifeTime,dir:mobile.floodFill.dir});
	}
}

function dark(mobile,position){
	
}

crafts=[
function(datas){if(datas['red']){return [['red'],'red_fuel']}return false},
function(datas){if(datas['blue']){return [['blue'],'blue_fuel']}return false},
function(datas){if(datas['green']){return [['green'],'green_fuel']}return false},
];


events={
	'craft':function(mobile){
		
		for(var c in crafts){
			var craft=crafts[c](mobile.position.datas);

			if(craft){
				for(var i in craft[0]){
					mobile.position.fill({bool:false,data:craft[0][i]});
				}
				mobile.position.fill({owner:mobile,data:craft[1]});
				mobile.position.log(parseLog('crafted:'+craft[1]),1000);
					//events['drop_one'](mobile);
					return true
				}
			}
			mobile.position.log(parseLog('nothing to craft'),1000);
		},
		'emit':function(mobile){
			if(!mobile.inventory.length){
				animations['negatif'](mobile);
				mobile.position.log(parseLog('inventory:empty'),1000);
				return false
			}
			animations['positif'](mobile);
			animations['drop'](mobile);
			var flood = Object.assign({}, mobile.floodFill);
			flood.data=mobile.inventory[0];
			flood.bool=true;
			mobile.position.floodFill(flood);
			mobile.position.log(parseLog('emit:'+mobile.inventory[0]),1000);
			mobile.inventory.splice(0,1);
		},
	'drop':function(mobile){
		if(!mobile.inventory.length){
			animations['negatif'](mobile);
			mobile.position.log(parseLog('inventory:empty'),1000);
			return false
		}
		animations['drop'](mobile);
		var flood = Object.assign({}, mobile.floodFill);
		flood.floodEnd=floodEnds['drop'];
		flood.bool=true;
		mobile.position.floodFill(flood);
		mobile.position.log(parseLog('drop'),1000);
	},
	'drop_all':function(mobile){
		if(!mobile.inventory.length){
			animations['negatif'](mobile);
			mobile.position.log(parseLog('inventory:empty'),1000);
			return false
		}
		animations['drop'](mobile);
		var flood = Object.assign({}, mobile.floodFill);
		flood.floodEnd=floodEnds['drop'];
		flood.lifeTime=mobile.inventory.length;
		flood.bool=true;
		mobile.position.floodFill(flood);
		mobile.position.log(parseLog('all'),1000);
	},
	'choose_to_vacuum':function(mobile){
		var data=false;
		for(var i in mobile.position.datas){
			if(mobile.position.datas[i] && mobile.datas[i] && mobile.datas[i]['receivable'] && i != mobile.floodFill.data){
				mobile.vacuum=i; 
				animations['positif'](mobile);
				data=i;
				mobile.position.log(parseLog('vacuum:'+data),1000);
				return true
			}
		}
		if(!data){
			animations['negatif'](mobile);
			mobile.position.log(parseLog('no valid data'),1000);
			return false
		}	
	},
	'vacuum':function(mobile){
		if(!mobile.vacuum){mobile.position.log(parseLog('SPACE:choose vacuum'),1000);return false;}
		var flood = Object.assign({}, mobile.floodFill);
		flood.data=mobile.vacuum;
		flood.floodEnd=floodEnds['vacuum'];
		flood.bool=false;
		animations['vacuum'](mobile);
		
		if(mobile.inventory.length<mobile.capacity){
			mobile.position.log(parseLog('vacuum'),1000)
		}else{
			animations['negatif'](mobile);
			mobile.position.log(parseLog('inventory:full'),1000);
		}
		mobile.position.floodFill(flood);
	},
	'strike':function(mobile){
		if(!mobile.inventory.length)return false
			var strike = Object.assign({}, mobile.floodFill);
		strike.data='punch';
		strike.mobile=mobile;
		strike.animation='fire';
		strike.delay=100;
		strike.bool=true;
		event(strike);
		mobile.inventory.splice(0,1);
	},
	'darkness':function(mobile,position){
		animation({mobile:mobile,data:'night',bool:false,speed:1,floodEnd:floodEnds['light'],scope:obstacles,lifeTime:16});
		if(mobile.position.position-position.position<8 && mobile.position.position-position.position>-8
			&& mobile.position.context[1].position-position.context[1].position<8 && mobile.position.context[1].position-position.context[1].position>-8){
			setTimeout(function(){
				dark(mobile,position);
			},100);
	}else{
		position.floodFill({floodEnd:floodEnds['light'],data:'night',scope:obstacles,bool:true,speed:1,lifeTime:16});
	}
}
}


eventsKeys={
	'player1':{
		'z':function(mobile){
			mobile.move([0,-1,0]);
		},
		'q':function(mobile){
			mobile.move([-1,0,0])
		},
		's':function(mobile){
			mobile.move([0,1,0])
		},
		'd':function(mobile){
			mobile.move([1,0,0])
		},
		'a':function(mobile){
			events['drop'](mobile);
		},
		'e':function(mobile){
			events['vacuum'](mobile);
		},
		' ':function(mobile){
			events['choose_to_vacuum'](mobile);
		},
		'Shift':function(mobile){
			events['craft'](mobile);
		},

	},
	'dead':{
	}
}

var directionsToKeys={
	'-1,1,0':'1',
	'0,1,0':'2',
	'1,1,0':'3',
	'-1,0,0':'4',
	'1,0,0':'6',
	'-1,-1,0':'7',
	'0,-1,0':'8',
	'1,-1,0':'9',
}
var keysToDirection={
	'1':[-1,1,0],
	'2':[0,1,0],
	'3':[1,1,0],
	'4':[-1,0,0],
	'6':[1,0,0],
	'7':[-1,-1,0],
	'8':[0,-1,0],
	'9':[1,-1,0],
}

setups={
	'default':function(){
		var screen = new Scale({dom:'table',class:'screen'});
		screen.buildIn(18,{class:'line',dom:'tr'});
		for(var l in screen.line)screen.line[l].buildIn(48,{class:'type',dom:'td'});
			for(var l in screen.line)for(var t in screen.line[l].type)screen.line[l].type[t].dom.textContent=' ';

				space = new Scale({class:'space'});
			space.buildIn(4,{class:'layer'});
			space.layer[0].buildIn(18,{class:'line'});
			for(var l in space.layer[0].line)space.layer[0].line[l].buildIn(48,{class:'type'})
				for(var l in space.layer[0].line){
					for(var t in space.layer[0].line[l].type){
						space.layer[0].line[l].type[t].domOutput=screen.line[l].type[t];
						screen.line[l].type[t].input=space.layer[0].line[l].type[t];
					}
				}
				return {
					'space':space,
					'screen':screen
				}

			},
		}

		datas={
			'player1':{
				'red':{
					'receivable':true,
					//'lifeBonus':true,
				},
				'green':{
					'receivable':true,
				},
				'blue':{
					'receivable':true,
				},
				'red_fuel':{
					'receivable':true,
					//'lifeBonus':true,
				},
				'green_fuel':{
					'receivable':true,
				},
				'blue_fuel':{
					'receivable':true,
				},
				'punch':{
					'deadly':true,
				},
			},
			'monster0':{
				'red':{
					'deadly':true,
				}
			}
		}

		dataToMobileEvents={
			'deadly':function(mobile,data){
				animations['hit'](mobile);
				mobile.position.fill({data:mobile.class,bool:false});
				mobile.dead=true;
			},
			'receivable':function(mobile,data){
				mobile.inventory.push(data);
			},
			'dir0':function(mobile,data){
				mobile.floodFill.dir.push([1,0,0]);
			},
			'lifeBonus':function(mobile,data){
				//mobile.position.log('lifeTime bonus!',false,[0,-1,0]);
				mobile.floodFill.lifeTime++;
			},
		}

		solids=['wall','turnet','mobile','player'];
		obstacles=['wall'];
		nonConductor=['wall'];

		function floodMobiles(options,mobiles,path){
			for(var m in mobiles){
				if(options.owner!=mobiles[m] && mobiles[m]){
					console.log(mobiles[m],options.data);
					if(mobiles[m].datas[options.data])for(var i in mobiles[m].datas[options.data])dataToMobileEvents[i](mobiles[m],options.data);
				}
			}
		}

		function floodEnding(scale,options,floodEnd,i){
			if(!i)i=0;
			if(i>floodEnd.length-1)return true
				if(!floodEnd[i](scale,options))return false;
			i++;
			return floodEnding(scale,options,floodEnd,i);
		}

		function parseFloodFill(options,setting){
			if(!setting)setting=floodFills['default'];
			fill=define({},options,setting);
			return fill;
		}

		logParsing={
			' ':'log_space',
			'?':'log_int',
			'!':'log_excl',
			'.':'log_dot',
			',':'log_comma',
			'<':'log_smaller',
			':':'log_dotdot',
		}

		function parseLog(str){
			var logArray=[];
			for(var i in str){
				if(logParsing[str.charAt(i)]){
					logArray.push(logParsing[str.charAt(i)]);
				}else{
					//console.log('log_'+str.charAt(i));
					logArray.push('log_'+str.charAt(i));
				}
			}
			return logArray;
		}