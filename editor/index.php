<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		table{
			border-collapse: collapse;
			margin-bottom: 2rem;
			background-color: grey;
		}
		td{
			width: 2rem;
			height: 2rem;
			border-left: solid;
			border-top: solid;
			border-width: 1px;
			border-color: rgba(0,0,0,0.06125)
		}
	</style>
</head>
<body>
	<script type="text/javascript">
		var colorpicked='';
		var userPosition=[0,0];
		var color=[0,0,0];
		var selected=false;
		var matrix = document.createElement('table')
		var y = 0
		var pixels=[];

		while(true){
			var line = document.createElement('tr')
			line.className='y';
			var x = 0
			while(true){
				var point = document.createElement('td')
				pixels.push([0,0,0])
				line.appendChild(point);
				x++
				if(x>7)break
			}
			matrix.appendChild(line)
			y++
			if(y>7)break
		}

		document.body.appendChild(matrix);

		var palette = document.createElement('table')
		var line = document.createElement('tr')
		var x = 0
		while(true){
			var point = document.createElement('td')
			line.appendChild(point);
			x++
			if(x>7)break
		}
		palette.appendChild(line)
		document.body.appendChild(palette);

		function update(){
			var lines=document.getElementsByTagName('tr');
			for(var i in userPosition){
				if(userPosition[i]<0)userPosition[i]=0;
			}
			if(userPosition[0]>lines.length-1)userPosition[0]=lines.length-1;
			var points=lines[userPosition[0]].getElementsByTagName('td');
			if(userPosition[1]>points.length-1)userPosition[1]=points.length-1;
			if(selected){
				if(selected.backgroundColor!=undefined){selected.style.backgroundColor='rgb('+selected.backgroundColor[0]+','+selected.backgroundColor[1]+','+selected.backgroundColor[2]+')'}else{selected.style.backgroundColor='transparent'};
			}
			selected=points[userPosition[1]]
			selected.style.backgroundColor='rgb('+color[0]+','+color[1]+','+color[2]+')'

			var json = {'something':'caca'};
			var data = JSON.stringify(json);

			var xhr = new XMLHttpRequest();

			xhr.open("POST", "script.php", false);

			xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

			xhr.send(data);

			if (xhr.status === 200) {
			    console.log("Réponse reçue: %s", xhr.responseText);
			} else {
			    console.log("Status de la réponse: %d (%s)", xhr.status, xhr.statusText);
			}

		}

		update();

		document.onkeydown=function(e){
			if(e.key=='d')userPosition[1]++
			if(e.key=='q')userPosition[1]--
			if(e.key=='z')userPosition[0]--
			if(e.key=='s')userPosition[0]++
			if(e.key=='r')if(color[0])color[0]-=32
			if(e.key=='v')if(color[1])color[1]-=32
			if(e.key=='b')if(color[2])color[2]-=32
			if(e.key=='R')if(color[0]<256)color[0]+=32
			if(e.key=='V')if(color[1]<256)color[1]+=32
			if(e.key=='B')if(color[2]<256)color[2]+=32
			if(e.key=='p'){selected.backgroundColor=[].concat(color)}
			if(e.key=='c'){if(selected.backgroundColor!=undefined)color=[].concat(selected.backgroundColor)}
			for(var i in document.body.getElementsByTagName('table')[1].getElementsByTagName('tr')[0].getElementsByTagName('td')){
				x=document.body.getElementsByTagName('table')[1].getElementsByTagName('tr')[0].getElementsByTagName('td')[i]
				if(e.key==i)console.log(x.backgroundColor)
				if(e.key==i && x.backgroundColor!=undefined)color=[].concat(x.backgroundColor)
			}
			update();
		}
		

	</script>
</body>
</html>