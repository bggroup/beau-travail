Mobile = function(options){
	var mobile=this;
	define(this,options,{
		jump:1,
		moveFlood:{data:'mobile',scope:['wall']},
		moveKeys:{
			'1':[-1,1],
			'x':[-1,1],
			'2':[0,1],
			'3':[1,1],
			'4':[-1,0],
			//'5':[0,0],
			'6':[1,0],
			'7':[-1,-1],
			'8':[0,-1],
			'9':[1,-1],
			'z':[0,-1],
			'q':[-1,0],
			's':[0,1],
			'd':[1,0],
			'ArrowUp':[0,-1],
			'ArrowLeft':[-1,0],
			'ArrowDown':[0,1],
			'ArrowRight':[1,0],
		},
		floodKeys:{
			'e':{owner:this,bool:false},
			'a':{owner:this,bool:true}
		},
		functionKeys:{
			'i':function(){console.log(mobile.inventory)},
			'5':function(e){
				for(var i in mobile.position.datas){
					if(mobile.position.datas[i] && !nonPullable[i]){
						mobile.floodKeys['e'].data=i;
					}	
				}
				mobile.position.floodFill({data:'choose',speed:17,lifeTime:2,rdm:0.5});
				setTimeout(function(){
					mobile.position.floodFill({data:'choose',speed:17,bool:false,lifeTime:4});
				},100);
			},
			'Backspace':function(e){
				console.log('empty');
				mobile.lock=0;
				mobile.position.floodFill({data:'push',speed:17,lifeTime:parseInt(mobile.inventory.length/4+1)});
				setTimeout(function(){
					mobile.position.floodFill({data:'push',speed:17,bool:false,lifeTime:parseInt(mobile.inventory.length/2+1)});
				},100);
				mobile.position.floodFill({owner:mobile,bool:true,lifeTime:parseInt(mobile.inventory.length)/4+1});
			},/*
			'Enter':function(e){
				console.log('getOne');
				mobile.lock=0;
				mobile.position.floodFill({data:'pull',speed:17,lifeTime:1});
				setTimeout(function(){
					mobile.position.floodFill({data:'pull',speed:17,bool:false,lifeTime:2});
				},100);
				var flood=mobile.floodKeys['e'];
				flood.lifeTime=1;
				mobile.position.floodFill(mobile.floodKeys['e']);
				
			},*/
			' ':function(e){
				var ingredients={};
				for(var i in mobile.position.datas){
					if(mobile.position.datas[i] && !nonPullable[i] && !nonIngredient[i]){
						ingredients[i]=true;
					}	
				}
				//console.log(ingredients);
				if(ingredients['red']&&ingredients['green']&&ingredients['blue']){
					mobile.inventory.push('money');
					for(var i in ingredients){
						mobile.position.fill({data:i,bool:false})
					}
					var e={key:'a'};
					mobile.events(e)
					return true;
				}
				if(ingredients['red']&&ingredients['money']){
					mobile.inventory.push('emitter');
					for(var i in ingredients){
						mobile.position.fill({data:i,bool:false})
					}
					var e={key:'a'};
					mobile.events(e)
					return true;
				}
				if(ingredients['money']&&ingredients['blue']){
					mobile.inventory.push('receiver');
					for(var i in ingredients){
						mobile.position.fill({data:i,bool:false})
					}
					var e={key:'a'};
					mobile.events(e)
					return true;
				}
				if(ingredients['red']&&ingredients['green']){
					mobile.inventory.push('jumper');
					for(var i in ingredients){
						mobile.position.fill({data:i,bool:false})
					}
					var e={key:'a'};
					mobile.events(e)
					return true;
				}
				if(ingredients['red']&&ingredients['blue']){
					mobile.inventory.push('memory');
					for(var i in ingredients){
						mobile.position.fill({data:i,bool:false})
					}
					var e={key:'a'};
					mobile.events(e)
					return true;
				}
				if(ingredients['red']){
					mobile.inventory.push('life');
					for(var i in ingredients){
						mobile.position.fill({data:i,bool:false})
					}
					var e={key:'a'};
					mobile.events(e)
					return true;
				}
				if(ingredients['blue']){
					mobile.inventory.push('space');
					for(var i in ingredients){
						mobile.position.fill({data:i,bool:false})
					}
					var e={key:'a'};
					mobile.events(e)
					return true;
				}
				if(ingredients['green']){
					mobile.inventory.push('lock');
					for(var i in ingredients){
						mobile.position.fill({data:i,bool:false})
					}
					var e={key:'a'};
					mobile.events(e)
					return true;
				}
				mobile.position.floodFill({data:'make',speed:17,lifeTime:2,rdm:0.5});
				setTimeout(function(){
					mobile.position.floodFill({data:'make',speed:17,bool:false,lifeTime:4});
				},100);
			}
		},
		capacity:10,
		lock:0,
		inventory:[],
		memory:1,
	});
onKeyDownEvents.push(function(e){
	mobile.events(e);
})
this.position.fill({data:'mobile'});
this.events({});
}

Mobile.prototype.events = function(e) {
	this.capacity=10;
	this.lock=0;
	this.jump=1;
	this.memory=1;
	this.floodKeys['a'].lifeTime=2;
	this.floodKeys['e'].lifeTime=2;
	var mobile=this;
	var position=this.position;
	for(var i in this.inventory){
		if(this.inventory[i]=='life'){this.floodKeys['a'].lifeTime++;this.floodKeys['e'].lifeTime++;};
		if(this.inventory[i]=='space')this.capacity=parseInt(this.capacity*1.25);
		if(this.inventory[i]=='lock')this.lock+=2;
		if(this.inventory[i]=='jumper')this.jump++;
		if(this.inventory[i]=='memory')this.memory+=2;
	}
	setTimeout(function(){
		if(mobile.position.id-position.id>(mobile.floodKeys['a'].lifeTime)*2 || mobile.position.id-position.id<-(mobile.floodKeys['a'].lifeTime)*2 || mobile.position.context.id-position.context.id>(mobile.floodKeys['a'].lifeTime)*2 || mobile.position.context.id-position.context.id<-(mobile.floodKeys['a'].lifeTime)*2
			){position.floodFill({data:'night',scope:['mobile','day'],lifeTime:mobile.floodKeys['a'].lifeTime+3})}else{
			mobile.events({});
		return;
	};
},this.memory*5000);
	this.position.floodFill({data:'night',bool:false,hard:true,speed:1,lifeTime:this.floodKeys['a'].lifeTime+1});
	if(this.moveKeys[e.key]){
		this.move(this.moveKeys[e.key]);
	}
	if(this.floodKeys[e.key]){


		//console.log(this.capacity);
		if(!this.floodKeys[e.key].bool){
			position.floodFill({data:'pull',speed:17,lifeTime:this.floodKeys[e.key].lifeTime});
			var time=this.floodKeys[e.key].lifeTime;
			setTimeout(function(){
				position.floodFill({data:'pull',speed:17,bool:false,lifeTime:time});
			},100);
		}else{
			position.floodFill({data:'push',speed:17,lifeTime:this.floodKeys[e.key].lifeTime});
			var time=this.floodKeys[e.key].lifeTime;
			setTimeout(function(){
				position.floodFill({data:'push',speed:17,bool:false,lifeTime:time});
			},100);
		}
		
		this.position.floodFill(this.floodKeys[e.key]);
		
	}
	if(this.functionKeys[e.key]){
		this.functionKeys[e.key](e);
	}
};

Mobile.prototype.move = function(mk){
	var mobile=this;
	this.position.floodFill({data:'jump',speed:17,lifeTime:this.jump,dir:[[mk[0],mk[1]]]});
	var position=this.position;
	setTimeout(function(){
		position.floodFill({data:'jump',speed:17,bool:false,lifeTime:mobile.jump*2});
	},100);
	try{
		if(matrix.line[this.position.context.id+this.jump*mk[1]].type[this.position.id+this.jump*mk[0]].fill(this.moveFlood)){
			this.moveFlood.bool=false
			this.position.fill(this.moveFlood);
			this.moveFlood.bool=true;
			this.position=matrix.line[this.position.context.id+this.jump*mk[1]].type[this.position.id+this.jump*mk[0]];
		}
	}catch(e){

	}
}

Mobile.prototype.update = function(bool){
}